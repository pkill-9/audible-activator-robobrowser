# audible-activator
Retrieves your activation data (activation_bytes) from Audible servers.

Runs on Python 3.

Dependencies: Robobrowser, lxml.

Heavily adapted from https://github.com/inAudible-NG/audible-activator

## Usage

Run ```$ ./audible-activator.py``` in your shell. then enter your account email and password when prompted.
To play the audiobooks:

```$ mpv --demuxer-lavf-o=activation_bytes=<your activation bytes> sample.aax```

```$ ffplay -activation_bytes <your activation bytes> sample.aax```

From [ArchWiki](https://wiki.archlinux.org/index.php/Audiobook#Audible_format): To convert the audiobooks into a non-DRM format:

```ffmpeg -activation_bytes <your activation bytes> -i <filename>.aax -vn -c:a copy <output>.mp4```

## Quick Setup

Python 3 is required along with [RoboBrowser](https://github.com/jmcarp/robobrowser) and lxml.

Using pip:

```pip install --user robobrowser lxml```

## Anti-Piracy Notice

Note that this project does NOT 'crack' the DRM. It simplys allows the user to
use their own encryption key (fetched from Audible servers) to decrypt the
audiobook in the same manner that the official audiobook playing software does.

Please only use this application for gaining full access to your own audiobooks
for archiving/converson/convenience. DeDRMed audiobooks should not be uploaded
to open servers, torrents, or other methods of mass distribution. No help will
be given to people doing such things. Authors, retailers, and publishers all
need to make a living, so that they can continue to produce audiobooks for us to
hear, and enjoy. Don’t be a parasite.

This blurb is borrowed from the https://apprenticealf.wordpress.com/ page.

## Debugging

If you see an error message like "audible_error=Internal service error has
occured while processing the request, please contact service admin" during the
activation process, then contact Audible customer care and they will clear up
your activation slots (there are 8 such slots).

If you see an "activation loop" in the official software (e.g. Audible Download
Manager), then you are seeing the same exact problem (you activation slots are
all used).

If this program does not work for you and all the debugging steps fail, then
use the https://github.com/inAudible-NG/tables project to recover your
"activation_bytes" in an offline manner.

The good news is that you need to retrieve your "activation_bytes" only once.

### AudibleActivation.sys

It is possible to extract the "activation_bytes" from an existing
'AudibleActivation.sys' file using AAS-parser from https://github.com/inAudible-NG/audible-activator

```
$ ./AAS-parser.py AudibleActivation.sys
B16B00B5
```

You can grab the 'AudibleActivation.sys' file from various already-activated
devices like Android phones, and Sansa music players.

## Credits
* inAudible-NG (https://github.com/inAudible-NG)
* kidburglar
