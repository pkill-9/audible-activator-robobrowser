#!/usr/bin/env python

import logging
import re
from getpass import getpass
import robobrowser
import base64, hashlib
from urllib.parse import urlparse, parse_qsl
from extractor import extract_activation_bytes


class Main():

    def __init__(self, email, password):
        self.username = email
        self.password = password
        self.browser = robobrowser.RoboBrowser(user_agent="Mozilla/5.0", parser="lxml")

    def _server_error_check(self, response):
        """Check Audible server response to see if it's an error"""
        if b"audible_error" in response.content: 
            raise Exception("Server response: {}".format(response.content.strip().decode()))

    def _login_response_check(self):
        captcha_image = self.browser.parsed.find("div", id="ap_captcha_img")
        error = self.browser.parsed.find("div", class_=re.compile("message*"), id=re.compile("message*"))
        if captcha_image != None:
            raise Exception("It asked for a CAPTCHA. submitting CAPTCHA's is currently unsupported.")
        if error != None:
            raise Exception("Failed to login. Amazon returned the following message: {}".format(error.text))

    def _server_communicate(self, action):
        url ='https://www.audible.com/license/licenseForCustomerToken'
        if action == "":
            payload = {"customer_token":self.playerToken}
        else:
            payload = {"customer_token":self.playerToken,"action":action}

        self.browser.open(url, params=payload)
        self._server_error_check(self.browser.response)
        return self.browser.response

    def _login_and_authenticate(self):
        # Step 0 setup
        logging.info("Step 0: setup")
        playerId = base64.encodebytes(hashlib.sha1("".encode()).digest()).rstrip()
        url = 'https://www.audible.com'
        payload = {"ipRedirectOverride":"true"}
        self.browser.open(url, params=payload)
        logging.debug("Browser URL: {}".format(self.browser.url))

        # Step 1 login to Audible
        logging.info("Step 1: login to Audible")
        url = "https://www.amazon.com/ap/signin" 
        payload = {
            'openid.ns': 'http://specs.openid.net/auth/2.0',
            'openid.identity': 'http://specs.openid.net/auth/2.0/identifier_select',
            'openid.claimed_id': 'http://specs.openid.net/auth/2.0/identifier_select',
            'openid.mode': 'logout',
            'openid.assoc_handle': 'amzn_audible_us',
            'openid.return_to': 'https://www.audible.com/player-auth-token?playerType=software&playerId={}=&bp_ua=y&playerModel=Desktop&playerManufacturer=Audible'.format(playerId)
        }
        self.browser.open(url, params=payload)
        form = self.browser.get_form()
        form["email"] = self.username
        form["password"] = self.password
        self.browser.submit_form(form)
        logging.debug("Browser URL: {}".format(self.browser.url))
        self._login_response_check()
    
        # Step 2 get authentication token
        logging.info("Step 2: get player authentication token")
        url = 'https://www.audible.com/player-auth-token'
        payload = {
            "playerType" : "software",
            "bp_ua" : "y",
            "playerModel" : "Desktop",
            "playerId" : playerId,
            "playerManufacturer" : "Audible",
            "serial" : ""
        }
        self.browser.open(url, params=payload)
        logging.debug("Browser URL: {}".format(self.browser.url))
        self.playerToken = dict(parse_qsl(urlparse(self.browser.url).query))["playerToken"] # Get player token from the url that was redirected to

        # Step 2.5, switch User-Agent to "Audible Download Manager"
        self.browser.session.headers["User-Agent"] = "Audible Download Manager"

    def fetch_activation_bytes(self):

        self._login_and_authenticate()
    
        # Step 3, de-register first, in order to stop hogging all activation slots (there are 8 of them!)
        logging.info("Step 3: de-register")
        self._server_communicate("de-register")
    
        # Step 4 get non-extracted activation bytes
        logging.info("Step 4: get raw activation-bytes page")
        activation_bytes_response = self._server_communicate("")
    
        # Step 5 (de-register again to stop filling activation slots)
        logging.info("Step 5: de-register again")
        self._server_communicate("de-register")

        # Step 6 extract activation bytes
        logging.info("Step 6: extract activation bytes")
        self.activation_bytes = extract_activation_bytes(activation_bytes_response.content)
        return self.activation_bytes
    

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO, format='%(message)s\n')
    username = input("Email address associated with Amazon account:")
    password = getpass("Password:")
    print()
    account = Main(username, password)
    account.fetch_activation_bytes()
    print()
    print("Activation bytes for {}:".format(username))
    print(account.activation_bytes)
